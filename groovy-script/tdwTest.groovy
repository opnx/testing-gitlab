import jenkins.*;
import jenkins.model.*;
import hudson.*;
import hudson.model.*;
import groovy.transform.Field;
import groovy.json.JsonOutput;

// Global Parameters
@Field final String tdwGitlabDomain = 'https://gitlab.com'; // with https://
@Field final String tdwFilePath= "files"
@Field final String tdwServiceNameFilter= "Pokemon"

def setServiceName(String id){
    def serviceList = [];

    withCredentials([[
        $class: 'UsernamePasswordMultiBinding',
        credentialsId: id,
        usernameVariable: '',
        passwordVariable: 'privateToken'
    ]]){
        String urlPerPage = '20';
        String urlRequest = "$tdwGitlabDomain/api/v4/projects?owned=true&simple=true&per_page=$urlPerPage&page=";
        String urlHealthCheck = sh(
            script: "curl -sk -o /dev/null -w \"%{http_code}\" -H 'PRIVATE-TOKEN: $privateToken' '$tdwGitlabDomain/api/v4/version'",
            returnStdout: true
        ).trim();
        
        if (urlHealthCheck == '200'){
            for (i=1; i != 0; i++) {
                String apiResponse = sh(
                    script: "curl -sk -H 'PRIVATE-TOKEN: $privateToken' '${urlRequest}${i}'",
                    returnStdout: true
                ).trim();
                if (apiResponse == "[]") break;
                def apiObject = readJSON(text: apiResponse);
                def serviceName = apiObject.name;
                serviceList += serviceName.findAll{
                    item -> item.contains(tdwServiceNameFilter)
                }
            }

            String json = JsonOutput.prettyPrint(JsonOutput.toJson(serviceList));
            writeFile(
                file: "$env.WORKSPACE/$tdwFilePath/newServiceName.json",
                text: json
            );
            String shaOldJson = sh(
                script: "sha256sum $env.WORKSPACE/$tdwFilePath/serviceName.json",
                returnStdout: true
            ).trim();
            String shaNewJson = sh(
                script: "sha256sum $env.WORKSPACE/$tdwFilePath/newServiceName.json",
                returnStdout: true
            ).trim();
            
            if (shaOldJson != shaNewJson){
                sh "mv $env.WORKSPACE/$tdwFilePath/newServiceName.json $env.WORKSPACE/$tdwFilePath/serviceName.json"
            }
            return "Tdw Groovy: Success Update serviceName.json"
        }else{
            return "Tdw Groovy: Error Code 0001"
        }
    }
}

// Get Service Name & Branch Name
def apiGetService(credentialsId){
    def objService = [];
    withCredentials([[
        $class: 'UsernamePasswordMultiBinding',
        credentialsId: credentialsId,
        usernameVariable: '',
        passwordVariable: 'privateToken'
    ]]){
        String urlPerPage = '20';
        String urlRequest = "$tdwGitlabDomain/api/v4/projects?owned=true&simple=true&per_page=$urlPerPage&page=";
        String urlHealthCheck = sh(
            script: "curl -sk -o /dev/null -w \"%{http_code}\" -H 'PRIVATE-TOKEN: $privateToken' '$tdwGitlabDomain/api/v4/version'",
            returnStdout: true
        ).trim();
        
        if (urlHealthCheck == '200'){
            for (i=1; i != 0; i++) {
                String apiResponse = sh(
                    script: "curl -sk -H 'PRIVATE-TOKEN: $privateToken' '${urlRequest}${i}'",
                    returnStdout: true
                ).trim();
                if (apiResponse == "[]") break;
                def apiObject = readJSON(text: apiResponse);
                def serviceName = apiObject.name;
                objService += serviceName.findAll{
                    item -> item.contains(tdwServiceNameFilter)
                }
            }
            return JsonOutput.prettyPrint(JsonOutput.toJson(objService));
        }else{
            return "Tdw Groovy: Error Code 0001"
        }
    }
}

return this