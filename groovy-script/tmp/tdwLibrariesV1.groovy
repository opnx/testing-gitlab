import jenkins.model.Jenkins;
import hudson.FilePath;

def tdwWorkspace() {
    return WORKSPACE
}

def getServiceName() {
    def fileContents = readJSON(file: "files/serviceName.json")
    return fileContents
}

return this