import jenkins.*;
import jenkins.model.*;
import hudson.*;
import hudson.model.*;
import groovy.json.*;
import groovy.transform.*;

class Student {
   private int StudentID;
   private String StudentName;
	
   void setStudentID(int pID) {
      StudentID = pID;
   }
	
   void setStudentName(String pName) {
      StudentName = pName;
   }
	
   int getStudentID() {
      return this.StudentID;
   }
	
   String getStudentName() {
      return this.StudentName;
   }
}
public accessStudent() {
    new Student()
}
return this