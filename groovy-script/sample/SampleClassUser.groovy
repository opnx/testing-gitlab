public class User {
    private String Firstname;
    private String Lastname;

    void setFirstname(String inputString) {
        Firstname = inputString;
    }

    void setLastname(String inputString) {
        Lastname = inputString;
    }

    String getFirstname() {
        return this.Firstname;
    }

    String getLastname() {
        return this.Lastname;
    }

    String getFullname() {
        return this.Firstname + ' ' + this.Lastname;
    }
}
public void user() {
    new User()
}
return this

// Sample Script
// script {
//     Object objClass = load("$WORKSPACE/files/SampleClassUser.groovy")
//     Object user = objClass.user()
//     user.setFirstname('Tdw')
//     user.setLastname('Devops')
//     println(user.getFullname())
// }