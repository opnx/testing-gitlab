import groovy.transform.Field;

@Field String htmlClassInput = 'setting-input';
@Field String htmlStyleInput = 'width: 600px;';
@Field String htmlStyleOptionSelected = 'font-style: italic;';
@Field String htmlClassOptionDefault = 'tdw-option-default';
@Field String htmlStyleDisabledInput = 'pointer-events: none;';
@Field String styleFallbackScript = """
    width: 600px;
    padding: 6px;
    font-style: italic;
    border-radius: 5px;
    border: 1px dashed #f00;
"""

def fallbackScript(Object obj) {
    def script = """
        html = '''
            <div style="$styleFallbackScript">${obj.inputText}</div>
        '''
        return html
    """
    return script
}
def selectService(Object obj) {
    def json = readJSON(file: "${obj.jsonFile}");
    def script = """
        html = '''
            <select name="value" class="$htmlClassInput" style="$htmlStyleInput">
            <option value="text-null" selected disabled style="$htmlStyleOptionSelected">Select Service</option>
        '''
    """
    for (i=0; i<json.size(); i++) {
        script += """
            html += '''
                <option value="${json[i].alias}">${json[i].name}</option>
            '''
        """
    }
    script += """
        html += '''
            </select>
        '''
        return html
    """
    return script
}
def selectBranch(Object obj) {
    def json = readJSON(file: "${obj.jsonFile}");
    def script = """
        html = '''
            <select name="value" class="$htmlClassInput" style="$htmlStyleInput">
        '''
        if (${obj.selectedParams} == "text-null") {
            html += '''
                <option value="text-null" selected disabled style="$htmlStyleOptionSelected">Select Branch</option>
            '''
        }
    """
    for (i=0; i<json.size(); i++) {
        script += """
            else if (${obj.selectedParams} == "${json[i].alias}") {
                html += '''
                    <option value="text-null" selected disabled style="$htmlStyleOptionSelected">Select Branch</option>
                '''
        """
        for (j=0; j<json[i].branch.size(); j++) {
        script += """
            html += '''
                <option value="${json[i].branch[j].name}">${json[i].branch[j].name}</option>
            '''
        """
        }
        script += """
            }
        """
        }
    script += """
        else {
            html += '<option value="text-unknown">Unknown Branch</option>'
        }
        html +='''
            </select>
        '''
        return html
    """
    return script
}
def selectNamespacePreprod(Object obj) {
    def script = """
        if ($obj.selectedParams) {
            html = '''
            <select name="value" class="${htmlClassInput}" style="${htmlStyleInput}" id="input-namespace">
            <option value="text-null" selected disabled style="${htmlStyleOptionSelected}">Select Namespace</option>
                <option value="tdw-dev">TDW Dev</option>
                <option value="tdw-staging">TDW Staging</option>
                <option value="tdw-preprod">TDW Preprod</option>
                <option value="tdw-webapi">TDW Webapi</option>
            </select>
            '''
        }
        return html
    """
    return script
}
def selectNamespaceProduction(Object obj) {
    def script = """
        if ($obj.selectedParams) {
            html = '''
            <select name="value" class="${htmlClassInput}" style="${htmlStyleInput}" id="input-namespace">
            <option value="text-null" selected disabled style="${htmlStyleOptionSelected}">Select Namespace</option>
                <option value="tdw-webapi">TDW Webapi</option>
                <option value="tdw-channel-a">TDW Channel A</option>
                <option value="tdw-channel-b">TDW Channel B</option>
            </select>
            '''
        }
        return html
    """
    return script
}
// @Field String styleFallbackSummary = """
//     width: 600px;
//     padding: 6px;
//     font-style: italic;
//     border-radius: 5px;
//     border: 1px dashed #f00;
// """
// def fallbackSummary() {
//     def script = """
//         html = '''
//             <div>Fallback Summary</div>
//         '''
//         return html
//     """
//     return script
// }
// def finalSummary() {
//     def defaultHtml = '''
//         html = """
//             <div style=\"
//                 width: 600px;
//                 padding: 10px;
//                 border-radius: 12px;
//             \">
//                 <table style=\"
//                     --table-padding: 0.55rem;
//                     background: #f4f4f8;
//                     border: solid #f4f4f8;
//                     border-radius: 12px;
//                     border-spacing: 0 2px;
//                     border-width: 5px 5px 3px;
//                     width: 100%;
//                 \">
//                     <tr>
//                         <th style=\"
//                             width: 200px;
//                         \">Parameter</th>
//                         <th style=\"
//                             width: 1px;
//                         \">:</th>
//                         <th style=\"
//                             width: 399px;
//                         \">Value</th>
//                     </tr>
//                     <tr>
//                         <td>Service Name</td><td>:</td><td>${Select_Service}</td>
//                     </tr>
//                     <tr>
//                         <td>Branch Name</td><td>:</td><td>${Select_Branch}</td>
//                     </tr>
//                     <tr>
//                         <td>Branch Name</td><td>:</td><td>${Select_Namespace}</td>
//                     </tr>
//                 </table>
//             </div>
//         """
//     '''
//     def script = """
//         if (Select_Service != 'text-null' && Select_Branch != 'text-null' && Select_Namespace != 'text-null') {
//     """
//     script += defaultHtml
//     script += """
//         } else {
//             html = '''
//                 <h1>Test</h1>
//             '''
//         }
//         return html
//     """
//     return script
// }
// def testSummary(Object obj) {
//     String valueService = obj.selectedService
//     String valueBranch = obj.selectedBranch
//     String valueNamespace = obj.selectedNamespace
//     def script = """
//         html = '''
//             <h1>Head</h1>
//         '''
//         if ($valueService != 'text-null' && $valueBranch != 'text-null' && $valueNamespace != 'text-null') {
//             html += '''
//                 <span>Masuk</span>
//             '''
//         } else {
//             html += '''
//                 <span>Keluas</span>
//             '''
//         }
//         return html
//     """
//     return script
// }

return this