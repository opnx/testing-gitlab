// import jenkins.*;
// import jenkins.model.*;
// import hudson.*;
// import hudson.model.*;

// import jenkins.model.Jenkins;
// import hudson.FilePath;
// import com.cloudbees.plugins.credentials.CredentialsProvider;
// import com.cloudbees.plugins.credentials.common.StandardUsernameCredentials;



import groovy.transform.Field;
import groovy.json.JsonOutput;

import groovy.transform.SourceURI
import java.nio.file.Path
import java.nio.file.Paths

@Field final String pathJson = 'files';
@Field final String serviceJson = 'listService.json';

def parseEnv(){
    @SourceURI
    URI sourceUri

    Path scriptLocation = Paths.get(sourceUri)
    println(scriptLocation)
    println(scriptLocation.getParent())
    println(scriptLocation.resolveSibling('tools.gvy'))
}


// evaluate(new File(".parsingEnv.groovy"))





def readJson(String pathWorkspace){
   return readJSON(file: "$pathWorkspace/$pathJson/$serviceJson");
}

def refreshServiceList(Object params){
    def objService = [];
    withCredentials([[
        $class: 'UsernamePasswordMultiBinding',
        credentialsId: params.credentialsId,
        usernameVariable: '',
        passwordVariable: 'privateToken'
    ]]){
        String urlPerPage = '20';
        String urlRequest = "$params.urlDomain/api/v4/projects?owned=true&simple=true&per_page=$urlPerPage&page=";
        String urlHealthCheck = sh(
            script: "curl -sk -o /dev/null -w \"%{http_code}\" -H 'PRIVATE-TOKEN: $privateToken' '$params.urlDomain/api/v4/version'",
            returnStdout: true
        ).trim();
        
        if (urlHealthCheck == '200'){
            for (i=1; i != 0; i++) {
                String apiResponse = sh(
                    script: "curl -sk -H 'PRIVATE-TOKEN: $privateToken' '$urlRequest$i'",
                    returnStdout: true
                ).trim();
                if (apiResponse == "[]") break;
                def apiObject = readJSON(text: apiResponse);
                def serviceName = apiObject.name;
                objService += serviceName.findAll{
                    item -> item.contains(params.filterServiceName);
                }
            }

            String json = JsonOutput.prettyPrint(JsonOutput.toJson(objService));
            String tmpJson = 'listServiceTmp.json'
            writeFile(
                file: "$params.pathWorkspace/$pathJson/$tmpJson",
                text: json
            );
            String shaOldJson = sh(script: "sha256sum $params.pathWorkspace/$pathJson/$serviceJson",returnStdout: true).trim();
            String shaNewJson = sh(script: "sha256sum $params.pathWorkspace/$pathJson/$tmpJson",returnStdout: true).trim();
            if (shaOldJson != shaNewJson){
                sh """
                    mv $params.pathWorkspace/$pathJson/$tmpJson $params.pathWorkspace/$pathJson/$serviceJson
                """
            }
            script{
                sh """
                    echo 'Push to Git'
                    cd $params.pathWorkspace
                """
                    // git config --local user.email '3746015-opnx@users.noreply.gitlab.com'
                    // git config --local user.name '3746015-opnx'
                    // git add $pathJson/$serviceJson
                    // git commit -m 'Auto genarate update from Jenkins'
                    // git push -u origin main
                    // git status
                    // git config -l
                    // git remote -v
                    // git checkout -b main
                    // git config --local user.email '3746015-opnx@users.noreply.gitlab.com'
                    // git config --local user.name '3746015-opnx'
                    // git add ${param.pathJson}/listService.json
                    // git commit -am 'Auto genarate update from Jenkins'
                    // git push origin main
                    // git config --local user.email '3746015-opnx@users.noreply.gitlab.com'
                    // git config --local user.name '3746015-opnx'
                    // git config --global user.email '3746015-opnx@users.noreply.gitlab.com'
                    // git config --global user.name '3746015-opnx'
            }
            println "Tdw Groovy: Success update file $serviceJson"
        }else{
            println "Tdw Groovy: Error Code 0001"
        }
    }
}

return this