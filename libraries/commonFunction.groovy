import groovy.json.JsonOutput;
// import groovy.yaml.YamlSlurper;
// import groovy.yaml.YamlBuilder;

def skipRunJob(String value) {
    println("Stage: '$value' has been skipped, because parameters are incomplete!");
}
def textSplit(String value) {
    Object objValue = value.split(',');
    for( String values : objValue )
    return values
}
def yamlToJson(String file) {
    def yaml = readYaml(file: file);
    return JsonOutput.prettyPrint(JsonOutput.toJson(yaml));
}
def createYaml(Object obj) {
    return writeYaml(data: obj.yaml, returnText: true)
    // return writeYaml(data: obj.yaml, file: obj.file, overwrite: true)
}
def genarateManifestService(Object obj) {
    def portDefault = 8080
config {
    apiVersion v1
    kind Service
    metadata {
        name "$obj.service"
        namespace "$obj.namespace"
        labels {
            app "$obj.service"
        }
    }
    spec {
        ports [
            {
                name "$obj.service"
                protocol TCP
                port "$portDefault"
                targetPort "$portDefault"
            }
        ]
        selector {
            app "$obj.service"
        }
    }
}

// def yaml = """---
// apiVersion: v1
// kind: Service
// metadata:
//   name: $obj.service
//   namespace: $obj.namespace
//   labels:
//     app: $obj.service
// spec:
//   ports:
//     - name: $obj.service
//       protocol: TCP
//       port: $portDefault
//       targetPort: $portDefault
//   selector:
//     app: $obj.service
// """
    // def yaml = readYaml(text: textYaml)
    // yaml.metadata.name = obj.service
    // yaml.metadata.namespace = obj.namespace
    // yaml.metadata.labels.app = obj.service
    // yaml.spec.ports[0].name = obj.service
    // yaml.spec.ports[0].port = portDefault
    // yaml.spec.ports[0].targetPort = portDefault
    // yaml.spec.selector.app = obj.service
    // return yaml
    return readYaml(text: config)
}
return this