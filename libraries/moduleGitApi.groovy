// import jenkins.*;
// import jenkins.model.*;
// import hudson.*;
// import hudson.model.*;
// import groovy.transform.Field;

import groovy.json.JsonOutput;

def urlHealthCheck(String urlDomain, String credentialsId){
    withCredentials([[
        $class: 'UsernamePasswordMultiBinding',
        credentialsId: credentialsId,
        usernameVariable: '',
        passwordVariable: 'privateToken'
    ]]){
        // privateToken = '0'
        String result = sh(script: "curl -sk -o /dev/null -w \"%{http_code}\" -H 'PRIVATE-TOKEN: ${privateToken}' '${urlDomain}/api/v4/version'", returnStdout: true).trim();
        if (result == '200'){
            return true
        }else{
            println('[Tdw_Groovy: "Error Code 0001"]')
            return false
        }
    }
}

// getRepository(
//     [
//         credentialsId: '76f96a13-f75f-46fa-8a4f-9c64160efc20',
//         gitlabDomain: 'https://gitlab.com',
//         outputDirectory: 'files',
//         filterService: 'service-',
//         perPage: '100'
//     ]
// );
def getRepository(Object tdw){
    Object result = [];
    Object serviceName = [];
    withCredentials([[
        $class: 'UsernamePasswordMultiBinding',
        credentialsId: tdw.credentialsId,
        usernameVariable: '',
        passwordVariable: 'privateToken'
    ]]){
        println('Get Repository Service');
        final String urlRequestService = "$tdw.gitlabDomain/api/v4/projects?owned=true&simple=true&per_page=${tdw.perPage}&page=";
        if (this.urlHealthCheck(tdw.gitlabDomain, tdw.credentialsId)) {
            for (i=1; i!=0; i++) {
                String urlResponseService = sh(script: "curl -sk -H 'PRIVATE-TOKEN: $privateToken' '${urlRequestService}${i}'", returnStdout: true).trim();
                if (urlResponseService == "[]") break;
                def jsonObjectService = readJSON(text: urlResponseService);
                for (j=0; j<jsonObjectService.size(); j++){
                    if ((jsonObjectService[j].path).contains(tdw.filterService)) {
                        result << [id: jsonObjectService[j].id, name: jsonObjectService[j].name, alias: jsonObjectService[j].path, branch: []];
                    }
                }
            }
            // println(JsonOutput.prettyPrint(JsonOutput.toJson(result)));
            // println('Count: ' + result.size());
        }
        if (this.urlHealthCheck(tdw.gitlabDomain, tdw.credentialsId)) {
            for (i=0; i<result.size(); i++){
                String urlRequestBranch = "$tdw.gitlabDomain/api/v4/projects/${result[i].id}/repository/branches";
                String urlResponseBranch = sh(script: "curl -sk -H 'PRIVATE-TOKEN: $privateToken' '$urlRequestBranch'", returnStdout: true).trim();
                def jsonObjectBranch = readJSON(text: urlResponseBranch);
                for (j=0; j<jsonObjectBranch.size(); j++){
                    result[i].branch << [name: jsonObjectBranch[j].name, commit: jsonObjectBranch[j].commit.id];
                }
            }
            String jsonRepository = JsonOutput.prettyPrint(JsonOutput.toJson(result));
            writeJSON(file: "$tdw.outputDirectory/Repository.json", json: jsonRepository);
        }
    }
}

return this